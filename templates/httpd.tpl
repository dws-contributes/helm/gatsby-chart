{{- define "gatsby.httpdshibd.settings" -}}
serverName: 'https://{{ .Values.route.prod }}:443'
shibEntityId: 'https://{{ .Values.route.prod }}'
httpdConf: 
  proxyTpl: "gatsby.httpdshibd.httpdconf.proxy"
{{- end -}}
{{- define "gatsby.httpdshibd.httpdconf.proxy" -}}
ProxyRequests Off
ProxyPass /Shibboleth.sso/ !
ProxyPass /health-check !

<Location /secure>
  AuthType Shibboleth
  Require shibboleth
  ShibRequestSetting requireSession 1
  require shib-session
</Location>

<LocationMatch "/page-data/secure.*">
  AuthType Shibboleth
  Require shibboleth
  ShibRequestSetting requireSession 1
  require shib-session
</LocationMatch>

{{- if eq .Values.solr.include_proxy true }}
ProxyPass "/solrsearch" "{{ .Values.solr.service_url }}"
ProxyPassReverse "/solrsearch" "{{ .Values.solr.service_url }}" 
{{ end -}}

# Redirect /icons directory
RedirectMatch ^/icons/(.*) /
# Rewrite URLs to lowercase if they don't resolve to a file/directory
RewriteEngine on
RewriteMap lowercase int:tolower
# Exclude Shibboleth
RewriteCond %{REQUEST_URI} !^/(Shibboleth.sso|download)
RewriteCond %{DOCUMENT_ROOT}%{REQUEST_URI} !-f
RewriteCond %{DOCUMENT_ROOT}%{REQUEST_URI} !-d
RewriteCond $1 [A-Z]
RewriteRule ^/?(.*)$ /${lowercase:$1} [R=301,L]
# Custom Server-side Redirects
{{- range .Values.shib_apache.redirects}}
{{.}}
{{- end }}

{{- end -}}

{{- define "gatsby.httpdshibd.deployment" -}}
{{- $settings := include "gatsby.httpdshibd.settings" . | fromYaml -}}
metadata:
  labels:
    app: {{ template "gatsby.namespaceAndProject" . }}
  name: httpd-gatsby-{{ template "gatsby.namespaceAndProject" . }}
spec:
  strategy: 
    type: RollingUpdate
  template:
    spec:
      containers:
        -
          {{- include "httpdshibd.render" (list . "gatsby" "container.httpd") | nindent 10 }}
        -
          {{- include "httpdshibd.render" (list . "gatsby" "container.shibd") | nindent 10 }}
      volumes:
        {{- include "httpdshibd.deployment.volumes" . | nindent 8 }}
        - name: static-files
          persistentVolumeClaim:
            claimName: gatsby-public-built-{{ template "gatsby.namespaceAndProject" . }}
        - name: shib-certs
          secret:
            defaultMode: 420
            items:
              - key: tls.crt
                path: tls.crt
              - key: tls.key
                path: tls.key
            secretName: gatsby-{{ template "gatsby.namespaceAndProject" . }}-shib-certs
{{- end -}}

{{- define "gatsby.httpdshibd.service" -}}
metadata:
  name: httpd-gatsby-{{ template "gatsby.namespaceAndProject" . }}
{{- end -}}

{{- define "gatsby.httpdshibd.container.httpd" -}}
resources:
  requests:
    cpu: 50m
image: "docker-registry.default.svc:5000/{{ .Values.shib_apache.namespace }}/{{ .Values.shib_apache.http_image_name }}:{{ .Values.shib_apache.http_image_tag }}"
volumeMounts:
  {{- include "httpdshibd.container.httpd.volumemounts" . | nindent 2 }}
  - name: static-files
    mountPath: /var/www/html
{{- end -}}

{{- define "gatsby.httpdshibd.container.shibd" -}}
resources:
  requests:
    cpu: 50m
image: "docker-registry.default.svc:5000/{{ .Values.shib_apache.namespace }}/{{ .Values.shib_apache.shib_image_name }}:{{ .Values.shib_apache.shib_image_tag }}"
volumeMounts:
  {{- include "httpdshibd.container.shibd.volumemounts" . | nindent 2 }}
  - name: shib-certs
    mountPath: /etc/shibboleth/tls/tls.crt
    subPath: tls.crt
  - name: shib-certs
    mountPath: /etc/shibboleth/tls/tls.key
    subPath: tls.key
{{- end -}}

{{ include "httpdshibd.render" (list . "gatsby" "configmap" (dict "overrideTpl" "-")) }}
---
{{ include "httpdshibd.render" (list . "gatsby" "service") }}
---
{{ include "httpdshibd.render" (list . "gatsby" "deployment") }}
