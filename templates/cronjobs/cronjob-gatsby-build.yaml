{{- if eq .Values.enable_cronjobs true }}
apiVersion: batch/v1beta1
kind: CronJob
metadata:
  name: gatsby-{{ .Values.okd_application_name }}-build
spec:
  schedule: {{ .Values.cronjob_schedules.build_job_schedule | quote }}
  concurrencyPolicy: Forbid
  successfulJobsHistoryLimit: 1
  failedJobsHistoryLimit: 1
  startingDeadlineSeconds: 180
  jobTemplate:
    spec:
      backoffLimit: 3
      template:
        spec:
          activeDeadlineSeconds: 1800
          containers:
          - name: gatsby-build
            image: {{ .Values.image.registry }}/{{ .Values.image.nameSpace }}/{{ .Values.image.name }}:{{ .Values.image.tag }}
            imagePullPolicy: Always
            command:
              - "/usr/local/bin/node"
            args:
              - "/usr/local/bin/npm"
              - "run-script"
              - "build"
              - "--"
              - "--verbose"
              - "--log-pages"
            env:
              - name: DRUPAL_BASE_URL
                value: {{ .Values.drupal_base_url }}
              - name: CDN_BASE_URL
                value: https://{{ .Values.route.staticFiles }}
              - name: CI
                value: 'true'
              - name: GATSBY_CPU_COUNT
                value: {{ .Values.gatsby_cpu_count | quote }}
              - name: FAST_BUILDS
                value: {{ .Values.fast_builds | quote }}
              - name: NODE_OPTIONS
                value: --max_old_space_size={{ .Values.node_max_memory }}
              - name: GATSBY_EXPERIMENTAL_PAGE_BUILD_ON_DATA_CHANGES
                value: 'true'
              - name: CONCURRENT_FILE_REQUESTS
                value: {{ .Values.concurrent_file_requests | quote }}
              - name: SITE_URL
                value: https://{{ .Values.route.prod }}
              - name: BASIC_AUTH_USERNAME
                valueFrom:
                  secretKeyRef:
                    key: username
                    name: basic-auth-{{ template "gatsby.namespaceAndProject" . }}
              - name: BASIC_AUTH_PASSWORD
                valueFrom:
                  secretKeyRef:
                    key: password
                    name: basic-auth-{{ template "gatsby.namespaceAndProject" . }}
              - name: SITE_GA_ID
                value: {{ .Values.site_ga_id }}
              - name: SITE_GTM_ID
                value: {{ .Values.site_gtm_id }}
              - name: SOLR_SERVER_URL
                value: {{ .Values.solr.server_url }}
              - name: SOLR_REWRITTEN_PATH
                value: {{ .Values.solr.rewritten_path }}
            resources:
              {{- toYaml .Values.resources | nindent 14 }}
            volumeMounts:
              - mountPath: /usr/src/app/public
                name: public
              - mountPath: /usr/src/app/.cache
                name: volume-gatsby-cache
              - mountPath: /.config
                name: volume-gatsby-config
          restartPolicy: Never
          volumes:
            - name: public
              persistentVolumeClaim:
                claimName: gatsby-public-built-{{ template "gatsby.namespaceAndProject" . }}
            - name: volume-gatsby-cache
              persistentVolumeClaim:
                claimName: gatsby-cache-build-{{ template "gatsby.namespaceAndProject" . }}
            - name: volume-gatsby-config
              persistentVolumeClaim:
                claimName: gatsby-config-build-{{ template "gatsby.namespaceAndProject" . }}
{{- end }}
