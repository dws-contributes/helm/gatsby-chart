#!/bin/bash

HELM_COMMAND=${HELM_COMMAND:-helm}

if [[ ! -f Chart.yaml ]]; then
    echo "This doesn't seem to be a chart directory" > /dev/stderr
    exit 1
fi

mkdir -p public

git fetch --tags

git tag | while read -r TAG; do
    mkdir -p build/${TAG}
    git archive ${TAG} | tar -xf - -C build/${TAG}
    find build/${TAG} -name 'Chart.yaml' | while read CHART_FILE; do
        ${HELM_COMMAND} package -d public $(dirname "$CHART_FILE")
    done
done

${HELM_COMMAND} repo index public
