{{/*
Default render settings.
*/}}
{{- define "httpdshibd.defaultsettings" -}}
serverName: null
objectNames:
  configMap: '{{ include "httpdshibd.defaultname" . }}'
  service: '{{ include "httpdshibd.defaultname" . }}'
  deployment: '{{ include "httpdshibd.defaultname" . }}'
httpdHealthCheck:
  aliasPath: /health-check
httpdConf:
  rootLocationTpl: "httpdshibd.httpdconf.rootlocation"
  htmlDirectoryTpl: "httpdshibd.httpdconf.htmldirectory"
  requestHeaderTpl: "httpdshibd.httpdconf.requestheader"
  proxyTpl: "httpdshibd.httpdconf.proxy"
{{- end -}}


