{{/*
A default name for objects, which is just the fullname, suffixed by "-gunicorn"
*/}}
{{- define "httpdshibd.defaultname" -}}
{{- include "common.fullname" . -}}-httpdshibd
{{- end -}}

{{/*
Template for ConfigMap.
*/}}
{{- define "httpdshibd.configmap" -}}
kind: ConfigMap
apiVersion: v1
metadata:
  name: '{{ .settings.objectNames.configMap }}'
  labels:
    {{- include "common.labels.standard" . | nindent 4 }}
data:
  httpd.conf: |
    {{- include "httpdshibd.httpdconf" . | nindent 4 }}
  shibboleth2.xml: |
    {{- include "httpdshibd.shibboleth2xml" . | nindent 4 }}
  attribute-map.xml: |
    {{- include "httpdshibd.attributemap" . | nindent 4 }}
  health-check.html: |
    <html lang="en">
      <head><title>HTTPD Health Check</title></head>
      <body>
        <h1>Health Check</h1>
        <p>Health is OK</p>
      </body>
    </html>
{{- end -}}

{{/*
Template for HTTPD service
*/}}
{{- define "httpdshibd.service" -}}
apiVersion: v1
kind: Service
metadata:
  name: '{{ .settings.objectNames.service }}'
  labels:
    {{- include "common.labels.standard" . | nindent 4 }}
spec:
  selector:
    deployment: '{{ .settings.objectNames.deployment }}'
  ports:
  - port: 8080
    protocol: TCP
    targetPort: 8080
{{- end -}}

{{/*
Template for deployment volumes
*/}}
{{- define "httpdshibd.deployment.volumes" -}}
- name: config
  configMap:
    name: '{{ .settings.objectNames.configMap }}'
    defaultMode: 420
- name: shibd-socket
  emptyDir: {}
{{- end -}}

{{/*
Define volume mounts that are common to the httpd and shibd containers.
*/}}
{{- define "httpdshibd.container.commonvolumemounts" -}}
- name: shibd-socket
  mountPath: /var/run/shibboleth
- name: config
  mountPath: /etc/shibboleth/shibboleth2.xml
  subPath: shibboleth2.xml
- name: config
  mountPath: /etc/shibboleth/attribute-map.xml
  subPath: attribute-map.xml
{{- end -}}

{{/*
Template for HTTPD container volume mounts.
*/}}
{{- define "httpdshibd.container.httpd.volumemounts" -}}
{{- include "httpdshibd.container.commonvolumemounts" . }}
- name: config
  mountPath: /etc/httpd/conf.d/custom.conf
  subPath: httpd.conf
- name: config
  mountPath: /var/www/health-check/index.html
  subPath: health-check.html
{{- end -}}

{{/*
Template for HTTPD container volume mounts.
*/}}
{{- define "httpdshibd.container.shibd.volumemounts" -}}
{{- include "httpdshibd.container.commonvolumemounts" . }}
{{- end -}}

{{/*
Template for HTTPD container
*/}}
{{- define "httpdshibd.container.httpd" -}}
name: httpd
image: "gitlab-registry.oit.duke.edu/duke_openshift_users/community-supported-resources/duke-shibboleth/duke-modshib:httpd-24-centos7"
volumeMounts:
  {{- include "httpdshibd.container.httpd.volumemounts" .  | nindent 2 }}

readinessProbe:
  httpGet:
    path: '{{ trimSuffix "/" .settings.httpdHealthCheck.aliasPath }}/'
    port: 8080
    scheme: HTTP

livenessProbe:
  httpGet:
    path: /
    port: 8443
    scheme: HTTPS
{{- end -}}

{{/*
Defaults for shibd container
*/}}
{{- define "httpdshibd.container.shibd" -}}
name: shibd
image: "gitlab-registry.oit.duke.edu/duke_openshift_users/community-supported-resources/duke-shibboleth/duke-shibboleth:centos7"
volumeMounts:
  {{- include "httpdshibd.container.shibd.volumemounts" . | nindent 2 }}

readinessProbe:
  exec:
    command: ['pgrep', '-f', '/usr/sbin/shibd']

livenessProbe:
  exec:
    command: ['pgrep', '-f', '/usr/sbin/shibd']
{{- end -}}

{{/*
Template for deployment
*/}}
{{- define "httpdshibd.deployment" -}}
kind: 'Deployment'
apiVersion: 'apps/v1'
metadata:
  name: '{{ .settings.objectNames.deployment }}'
  labels:
    {{- include "common.labels.standard" . | nindent 4 }}
spec:
  selector:
    matchLabels:
      deployment: '{{ .settings.objectNames.deployment }}'

  template:
    metadata:
      labels:
        deployment: '{{ .settings.objectNames.deployment }}'
        app: '{{ include "common.name" . }}'
        release: '{{ .Release.Name }}'
    spec:
      volumes:
        {{- include "httpdshibd.deployment.volumes" . | nindent 8 }}
{{- end -}}

{{/*
Render a template.

Pass a list of arguments to this template:

- arg 1: The top-level context.
- arg 2: The template that provides settings.
- arg 3: The key of the of the template set to render.
- arg 4: A dict of settings that can be used to explicitly specify a template or settings override.
*/}}
{{- define "httpdshibd.render" -}}
  {{- $top := index . 0 | deepCopy -}}
  {{- $baseName := index . 1 -}}
  {{- $tplName := index . 2 -}}

  {{- $kwArgs := dict -}}
  {{- if gt (. | len) 3 -}}
    {{- $_ := mergeOverwrite $kwArgs (index . 3) -}}
  {{- end -}}

  {{- $settingsTpl := $kwArgs.settingsTpl | default (printf "%s.httpdshibd.settings" $baseName) -}}

  {{- $_ := set $top "settings" (include "httpdshibd.defaultsettings" $top | fromYaml) -}}
  {{- $_ := mergeOverwrite $top.settings (include $settingsTpl $top | fromYaml) -}}

  {{- $overrideTpl := $kwArgs.overrideTpl | default (printf "%s.httpdshibd.%s" $baseName $tplName) -}}

  {{- if not (eq $overrideTpl "-") -}}
    {{- $result := include (printf "httpdshibd.%s" $tplName) $top | fromYaml -}}
    {{- $overrides := dict -}}
    {{- $_ := mergeOverwrite $overrides (include $overrideTpl $top | fromYaml) -}}
    {{- $_ := mergeOverwrite $result ($overrides | default dict) -}}
    {{- mergeOverwrite $result ($overrides | default dict) | toYaml -}}
  {{- else -}}
    {{- include (printf "httpdshibd.%s" $tplName) $top -}}
  {{- end -}}
{{- end -}}

