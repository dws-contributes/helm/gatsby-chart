{{/* HTTPD Configuration */}}
{{- define "httpdshibd.httpdconf" -}}
ServerName "{{ required "serverName is required" .settings.serverName }}"
UseCanonicalName On
ServerTokens Prod

# Health check
Alias {{ .settings.httpdHealthCheck.aliasPath }} /var/www/health-check
<Directory /var/www/health-check>
  Options -Indexes
  Require all granted
</Directory>

<Location />
  {{- include .settings.httpdConf.rootLocationTpl . | nindent 2 }}
</Location>

<Directory /var/www/html>
  {{- include .settings.httpdConf.htmlDirectoryTpl . | nindent 2 }}
</Directory>

{{ include .settings.httpdConf.requestHeaderTpl . }}

{{ include .settings.httpdConf.proxyTpl . }}
{{- end -}}

{{/* Default Location conf for / */}}
{{- define "httpdshibd.httpdconf.rootlocation" -}}
AuthType Shibboleth
Require shibboleth
{{- end -}}

{{/* Default Directory conf for / */}}
{{- define "httpdshibd.httpdconf.htmldirectory" -}}
Options -Indexes
{{- end -}}

{{- define "httpdshibd.httpdconf.requestheader" -}}
<IfModule mod_headers.c>
  Header set X-XSS-Protection "1; mode=block"
  Header always set Strict-Transport-Security "max-age=63072000;"
  Header set X-Content-Type-Options "nosniff"
</IfModule>

Header always append X-Frame-Options deny

RequestHeader unset Host

# Prevent spoofing for platforms that normalize hyphens to underscores
RequestHeader unset REMOTE-USER

RequestHeader set EPPN expr=%{ENV:EPPN}
RequestHeader set REMOTE_USER expr=%{ENV:REMOTE_USER}
{{- end -}}

{{/* Proxy conf */}}
{{- define "httpdshibd.httpdconf.proxy" -}}
ProxyRequests Off
ProxyPass /Shibboleth.sso/ !
ProxyPass {{ .settings.httpdHealthCheck.aliasPath }} !
{{- end -}}
